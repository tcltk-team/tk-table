Source: tk-table
Maintainer: Tcl/Tk Debian Packagers <pkg-tcltk-devel@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: interpreters
Priority: optional
Build-Depends: debhelper-compat (= 13),
               tcl-dev,
               tk-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/tcltk-team/tk-table
Vcs-Git: https://salsa.debian.org/tcltk-team/tk-table.git
Homepage: https://github.com/wjoye/tktable
Rules-Requires-Root: no

Package: tk-table
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         ${tk:Depends}
Description: Table extension for Tcl/Tk
 Provides support for tables and matrices in Tcl/Tk. The basic features of the
 widget are:
  * multi-line cells
  * support for embedded windows (one per cell)
  * row & column spanning
  * variable width columns / height rows (interactively resizable)
  * row and column titles
  * multiple data sources ((Tcl array || Tcl command) &| internal caching)
  * supports standard Tk reliefs, fonts, colors, etc.
  * x/y scrollbar support
  * 'tag' styles per row, column or cell to change visual appearance
  * in-cell editing - returns value back to data source
  * support for disabled (read-only) tables or cells (via tags)
  * multiple selection modes, with "active" cell
  * multiple drawing modes to get optimal performance for larger tables
  * optional 'flashes' when things update
  * cell validation support
  * Works everywhere Tk does (including Windows and Mac!)
 .
 This package is sufficient to run and link against tkTable.
